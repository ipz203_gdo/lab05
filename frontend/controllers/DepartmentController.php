<?php


namespace frontend\controllers;


use frontend\models\Department;
use yii\web\Controller;


class DepartmentController extends Controller
{
    public function actionIndex()
    {
        $makeInt = function($n)
        {
            return [
                'id' => (int)$n['department_id'],
                'department_name' => $n['department_name'],
                'parent_id' => (int)$n['parent_id'],
                'type_id' => (int)$n['type_id'],
            ];
        };
        $departments = Department::find()
            ->select(['department_id', 'department_name', 'parent_id', 'type_id'])
            ->from('department')
            ->asArray()
            ->indexBy('department_id')
            ->all();

        $departments = array_map($makeInt, $departments);
        $departmentsTree = TreeController::formTree($departments);
        return $this->render('index', ['tree' => $departmentsTree]);
    }

}
