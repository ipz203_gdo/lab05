<?php


namespace frontend\controllers;


use frontend\models\Department;
use frontend\models\Tree;
use yii\web\Controller;

class TreeController extends Controller
{
    public function actionIndex()
    {

        $tree = new Tree();
        $categories = $tree->getCategories();
        $newTree = $this->formTree($categories);
        $resultString = $this->ShowCategories($newTree);
        return $this->render('index', ['arr'=>$resultString]);
    }

    public static function formTree($arr)
    {
        $tree = array();
        foreach ($arr as $id => &$node)
        {
            if(!$node['parent_id'])
            {
                $tree[$node['id']] =& $node;
            }
            else
            {
                $arr[$node['parent_id']]['children'][] =& $node;
            }
        }
        unset($node);
        return $tree;
    }
    function showMenuItem($category)
    {
        $cat = '<li>' . $category['name'];
        if (isset($category['children']))
        {
            $cat .= $this->ShowCategories($category['children']);
        }
        $cat .= '</li>';
        return $cat;
    }
    function ShowCategories($arr)
    {
        $result = "";
        foreach ($arr as $item) {
            $result .= $this->showMenuItem($item);
        }
        return '<ul>' . $result . '</ul>';
    }


}