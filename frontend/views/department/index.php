<body>
<script src="https://d3js.org/d3.v3.js" charset="utf-8"></script>
<script>

    let categories = <?= json_encode($tree); ?>;
    var margin = {top: 100, right: 25, bottom: 100, left: 50},
        width = 1400 - margin.left - margin.right,
        height = 600 - margin.top - margin.bottom;

    var tree = d3.layout.tree()
        .separation(function(a, b) {
            return a.parent === b.parent ? 1 : 1; })
        .children(function(d) {return d.children;})
        .size([width, height]);

    var svg = d3.select(".wrap")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g");

    var nodes = tree.nodes(categories[1]);

    var node = svg.selectAll(".node")
        .data(nodes)
        .enter()
        .append("g");

    node.append("rect")
        .attr("width", 200)
        .attr("height", 80)
        .attr("fill",function(d) {
            console.log(d);
            if (d.type_id == 3)
            {
                return "PaleTurquoise";
            }
            else if(d.type_id == 2){
                return 'Turquoise';
            }
            else{
                return 'LightCyan';
            }
        }) // color the nodes
        .attr("x", function(d) {return d.x;})
        .attr("y", function(d) {return d.y;});

    node.append("text")
        .attr("font-size", "16px")
        .attr("fill", "black")
        .attr("x", function(d) { return d.x + 100; })
        .attr("y", function(d) { return d.y + 45; })
        .style("text-anchor", "middle")
        .style('white-space', "pre-line")
        .text(function(d) { return d.department_name; });


    var link = svg.selectAll(".link")
        .data(tree.links(nodes))
        .enter()
        .insert("path", "g")
        .attr("fill", "none")
        .attr("stroke", "#000")
        .attr("stroke", "#000")
        .attr("shape-rendering", "crispEdges")
        .attr("d", connect);

    function connect(d, i) {
        return     "M" + (d.source.x + 100) + "," + (d.source.y)
             + "V" + (d.target.y - 25)
             + "H" + (d.target.x + 100)
             + "V" + (d.target.y);
    };

</script>
