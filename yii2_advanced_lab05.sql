-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июл 15 2022 г., 13:40
-- Версия сервера: 8.0.29
-- Версия PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `yii2_advanced_lab05`
--

-- --------------------------------------------------------

--
-- Структура таблицы `department`
--

CREATE TABLE `department` (
  `department_id` int NOT NULL,
  `department_name` varchar(255) NOT NULL,
  `parent_id` int DEFAULT NULL,
  `type_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Дамп данных таблицы `department`
--

INSERT INTO `department` (`department_id`, `department_name`, `parent_id`, `type_id`) VALUES
(1, 'Рада директорів', NULL, 1),
(2, 'Головна виконавча рада', 1, 1),
(3, 'Операційний центр', 2, 2),
(4, 'Фінансовий центр', 2, 2),
(5, 'Центр розробки', 2, 2),
(6, 'Відділ кадрів', 3, 3),
(7, 'Відділ продажів', 3, 3),
(8, 'Маркетинговий центр', 2, 2),
(9, 'Відділ аудиту', 4, 3),
(10, 'Відділ податків', 4, 3),
(11, 'Казначейський відділ', 4, 3),
(12, 'Рекламний відділ', 8, 3),
(13, 'Відділ аналізу потреб споживачів', 8, 3),
(14, 'Відділ зв\'язків з громадськістю', 8, 3),
(15, 'Відділ розробки', 5, 3),
(16, 'Відділ досліджень', 5, 3),
(17, 'Відділ внутрішньої інфраструктури', 5, 3),
(18, 'Відділ статистики', 3, 3),
(19, 'Відділ ризиків', 4, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `department_type`
--

CREATE TABLE `department_type` (
  `type_id` int NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Дамп данных таблицы `department_type`
--

INSERT INTO `department_type` (`type_id`, `name`) VALUES
(1, 'Board'),
(2, 'Center'),
(3, 'Department');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1656277515),
('m140506_102106_rbac_init', 1656277527),
('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1656277527),
('m180523_151638_rbac_updates_indexes_without_prefix', 1656277527),
('m200409_110543_rbac_update_mssql_trigger', 1656277527);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `first_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL,
  `department_id` int DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint NOT NULL DEFAULT '10',
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `verification_token` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `first_name`, `last_name`, `department_id`, `image`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `verification_token`) VALUES
(1, 'Jane', 'Jane', NULL, NULL, 'DBIyR9g5yhr011oZgiwU0-5eKQWLxnO6', '$2y$13$w6tQLfj1qtRQ8trH0t5f2.fm.QoMtNf7HmsNd111So4SRg2CruFLm', NULL, 'jane@gmail.com', 10, 1657216946, 1657216946, 'BKl9niptfXREv56FtUncRggX35L8TmBK_1657216946'),
(2, 'John', 'John', 1, '', '0VxRztRlmXgCxpUgy9MUrMSIqsSnUpdI', '$2y$13$e8ISFg5cTwXoEs.Ty4Btx.hpmd0HzwO8Zu2IOWqCFAv.6x.UuW8OG', NULL, 'john@gmail.com', 10, 1657216932, 1657216932, 'brAh8V64mkvaReusmgHaPugbphohx4O__1657216931'),
(3, 'Peter', 'Peter', NULL, '', 'WFlopO0dukPc0vr5Uzu0aYbijCxa3-bq', '$2y$13$HgEZgnVOZVxMbAHfaIfwNu59hJOL27Zyg7npH72QTCZsMK/M9UrOu', NULL, 'peter@gmail.com', 10, 1657216961, 1657216961, '4Eaa-KzmlOL1xCmDL7jMVYQEbbA42slJ_1657216961'),
(4, 'Alex', 'Alex', 1, 'iO6qIHILGvjKmi1XNvRY3T9Bx-Cf7BmW.png', 'WlYXlIx1D8oqZCO7_-3C-xVprXeZBQ56', '$2y$13$SsvYdLGJqm.fuFcOUfwhkuTZU9RypFyt0.m3NHiHHhiCO2SbAvhVy', NULL, 'alex@gmail.com', 10, 1657216975, 1657216975, 'ZHODRxWNrpfbCOa5S6vSj2vzZpX1TVLT_1657216975'),
(5, 'Roman', 'Kovban', 3, 'zszBYh67lvCGHSluiQAkHcxKBhWlZzKC.png', 'I0ZZyzB7HRySJAjChZ6XbdOD3oiXOylc', '$2y$13$vsVUVWdLEt9tYnKxrGdi8.L9Q7/ZuJPxjpIczAJR.MbjsTk.V4E7S', NULL, 'roman@gmail.com', 10, 1657402237, 1657402237, '5QtrkaiptXqpTWPuU1OQP4uAwcFhTppI_1657402237');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`department_id`);

--
-- Индексы таблицы `department_type`
--
ALTER TABLE `department_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`),
  ADD KEY `department_id` (`department_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `department`
--
ALTER TABLE `department`
  MODIFY `department_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT для таблицы `department_type`
--
ALTER TABLE `department_type`
  MODIFY `type_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`department_id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
